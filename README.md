# postLawson

postLawson is a post-processing tool for analysis of wind tunnel data
agains the Lawson criteria for pedestrian-level wind comfort.

Contact: uk.cfd@wsp.com

## Prerequisits

* Printing images will require an X11 client like XMing
* Requires numpy, pandas, matplotlib, yaml.

## Background

The script reads normalised mean and GEM data from csv files 'mean.csv'. 
'gem.csv' stored in the './speedups/' directory in the form:

|0      | 10    |  20    |  ... | 350    |
|-      | -     |  -     |  -   | -      | 
|r1,0   | r1,10 |  r1,20 |  ... | r1,350 |  
|...    | ...   |  ...   |  ... | ...    |
|rn,0   | ...   |  ...   |  ... | rn,350 |  

where rn,i is the ratio for direction i at probe n. 

These speedup ratios are combined with weibull coefficients from 'season.csv' 
stored in the './metData/' directory in the form:

|p0 | p10 | p20 | ... | p350 | 
|-  | -   |  -  |  -  | -    | 
|c0 | c10 | c20 | ... | c350 | 
|k0 | k10 | c20 | ... | c350 | 

the filenames can be anything, but typically are {allyear, summer, winter,
spring, autumn}. 

Then each probe is compared to the LDDC Lawson criteria for comfort
and safety:

|   Category        |  Threshold wind speed  | Colour     |
| ----------------- | ---------------------- | ---------- |
|   Sitting         |  4.0m/s for 5%hrs      | Dark blue  |
|   Standing        |  6.0m/s for 5%hrs      | Light blue |
|   Leisure walking |  8.0m/s for 5%hrs      | Green      |
|   Business walking|  10.0m/s for 5%hrs     | Yellow     |
|   Uncomfortable   |  >10.0m/s for 5%hrs    | Red        |
| ------------------|------------------------|----------- |
|   Safety warning  |  >15.0m/s for 1.1hr/yr | Yellow     |
|   Safety 15       |  >15.0m/s for 2.2hr/yr | Red        |
|   Safety 20       |  >20.0m/s for 2.2hr/yr | Black      |

Or can be compared to the city of london (CoL) guidance:

|   Category           |  Threshold wind speed  | Colour     |
| -------------------- | ---------------------- | ---------- |
|   Frequent sitting   |  2.5m/s for 5%hrs      | Grey       |
|   Occasional sitting |  4.0m/s for 5%hrs      | Blue       |
|   Standing           |  6.0m/s for 5%hrs      | Green      |
|   Walking            |  8.0m/s for 5%hrs      | Yellow     |
|   Uncomfortable      |  >8.0m/s for 5%hrs     | Red        |
| ---------------------|------------------------|----------- |
|   Safety             |  >15.0m/s for 2.2hr/yr | Red        |

These results can be summarised graphically if the user adds 


## Usage

postLawson is a standalone python tool which is executed from the command line
as follows:

```bash
postLawson.py [-h] [-l] [-p] [-c]
```
Options are:
* -h, --help: display the help file
* -l, --lplots: Switch to choose to enable the printing of Lawson plots. 
Default is False.
* -p, --pplots: Switch to print polar plots for gem, mean for each direction.
Default is False.
* -c, --col: Switch to choose to use the CoL version of the Lawson criteria. 
Defualt is False (i.e. LDDC criteria)

No arguments are requires as input, but the code expects the following input
files:
* weibull data: this must exist in metData/ and has three rows 
p, c, k, with as many columns as number of directions. Format must
be .csv and can be called anything, but best practice is {allyear, 
summer, winter, spring, autumn}
* coords: coords.csv is required in the running directory and has 
four columns: probe, x, y, z. Has as many rows as number of probes
* speedups: inside speedups/ need a means.csv and gem.csv with
as many columns as directions in weibull data, and as many rows as
probes in coords. 

Additional files are required for plotting lawson plots (-l):
* background.png: A background file for the graphical output of 
lawson plots. There is some nuance to creating these in order to ensure
that the spatial data matches the image data. The best way to do this 
is to use the Rhino CAD and do: 
    * Switch off all layers other than the buildings of interest
    * View 'Top' and Zoom to 0,0,0
    * Use the 'Print' command
    * Under Destination, change destination to 'Image File' and 
    change height, width and resolution as desired.
    * Under 'View and Output Scale' change the scale to ensure all 
    probes are within the image. You may have to adjust the height 
    and width to acheive this, or in some cases you may have to 
    change the CPlane to a better position to better capture all 
    probes.
    * Under 'Linetypes...'' line width looks best as 'Hairline'
* img.yml: This is a configuration file for the background.png. Take note 
of the settings you used to produce the background.png and put these in the
img.yml configuration file.


A brief description of the outputs:
* comfortSafety.csv. One of these will be produced for each of the csvs in 
metData/. For each probe, it gives a numerical grade {0, 1.5, ..., 4.5} for
comfort and safety. And for each criteria it gives % of hours (for comfort)
or number of hours/yr (for safety)
* polarPlots/* images of the polar plots for each probe.
* lawsonPlots/* graphical output of the lawson plots
