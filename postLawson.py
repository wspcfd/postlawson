"""postLawson.py: A post-processing tool for analysis of wind 
tunnel data against the Lawson criteria for pedestrian-level 
wind comfort.  

Requires numpy, pandas, matplotlib, yaml.

Required input files:

*weibull data: this must exist in metData/ and has three rows 
p, c, k, with as many columns as number of directions
*coords: coords.csv is required and has four columns: probe, 
x, y, z. Has as many rows as number of probes
*speedups: inside speedups/ need a means.csv and gem.csv with
as many columns as directions, and as many rows as probes

Optional input files:

*background image: in order to produce graphical output of the
lawson results, there must be a 'background.png' image. Refer to
README for info on its requirements
*img.yml: this is a configuration file for the background image
to ensure scaling is consistent with probes.

"""

import subprocess
import os
import numpy as np
import pandas as pd
import sys
import argparse
import math
import matplotlib.pyplot as plt
import shutil
from optparse import OptionParser
import yaml

def metdata(cwd):
    """ 
    Read met data in metData directory and ensure 
    it's in correct format
    """
    metDir = os.path.join(cwd, "metData/")
    if os.path.isdir(metDir):
        metList = []
        for sfile in os.listdir(metDir):
            name, extension = os.path.splitext(sfile)
            if extension == ".csv":
                metList.append(name)
            else:
                print ("# Warning: skipping "+ name +"as extension"
                        " is not .csv")
    else:
        sys.exit("### ERROR: The metData/ directory does not exist")
    if len(metList) < 1:
        sys.exit("### ERROR: There is no met data in metData/")
    
    return metList, metDir

def criteria(activity, col):
    """ 
    Define the threshold velocities for an activity depending
    on the version of lawson requested
    """
    if col:
        criteriaList = {
                "frequent sitting": [0.05, 2.5],
                "occasional sitting": [0.05, 4.0],
                "standing": [0.05, 6.0],
                "walking": [0.05, 8.0],
                "safety_15": [0.00022, 15.0]
                }
    else:
        criteriaList = {
                "sitting": [0.05, 4.0],
                "standing": [0.05, 6.0],
                "leisure walking": [0.05, 8.0],
                "business walking": [0.05, 10.0],
                "safety_w": [0.000125, 15.0],
                "safety_15": [0.00025, 15.0],
                "safety_20": [0.00025, 20.0]
                }

    Uthresh = criteriaList[activity][1]
    frequency = criteriaList[activity][0]
    
    return Uthresh, frequency

def readmet(filename):
    """ 
    Extract the p, c and k values from the met data
    """
    met = np.genfromtxt(filename, delimiter=",")
    p = met[0, :]
    if np.sum(p[:]) > 1.0:
        print ("Converting p to a ratio...")
        p = p * 0.01
    c = met[1, :]
    k = met[2, :]

    return p, c, k

def speedups(speeduppath):
    speedupdir = speeduppath #os.path.join(cwd, "speedups/")
    if os.path.isdir(speedupdir):
        meanU = np.genfromtxt(os.path.join(speedupdir, "means.csv"), 
                                delimiter=",", skip_header=1)
        gemU = np.genfromtxt(os.path.join(speedupdir, "gem.csv"), 
                                delimiter=",", skip_header=1)
        # Cover the possibility of user leaving first col in data
        if np.mean(meanU[:, 0]) > 2:
            print("Stripping first column from speedups...")
            meanU = meanU[:, 1::]
            gemU = gemU[:, 1::]
    else:
        sys.exit("### ERROR: The speedups directory is missing")

    return meanU, gemU

def combinedata(season, metDir, col, cwd):
    """Combines means, gems, from the experiment with the 
    weibull params to compare against the criteria

    Parameters
    _____________________________________________________
    season: str
        The seasonal met data to use in the analysis
    metDir: str
        Full path to the metData directory
    col: bool
        Switch to analyse the CoL (True) or LDDC (False) 
    variant of the lawson criteria
    cwd: string
        Current working directory for filenames

    Returns
    _____________________________________________________
    lawsonCSdf: pandas dataframe
        Dataframe with the comfort and safety gradings 
    for each probe, as well as no hours and % exceedance
    """
    print ("Doing stats for {}".format(season))
    p, c, k = readmet(os.path.join(metDir, season + ".csv"))
    if col:
        activity = ["frequent sitting", "occasional sitting", 
                    "standing", "walking", "safety_15"]
        # Define 'grade' values which assign an arbitrary numeric 
        # value to each activity so it can be plotted on a colorbar
        grade = [1.5, 2.5, 3.5, 4.5, 2.5]
    else:    
        activity = ["sitting", "standing", "leisure walking", 
                    "business walking", "safety_w", "safety_15",
                    "safety_20"]
        grade = [1.5, 2.5, 3.5, 4.5, 1.5, 2.5, 3.5]
    
    # Get maximum of mean and gem from speedups
    speeduppath = os.path.join(cwd, "./speedups")
    magU = np.maximum(speedups(speeduppath)[0], 
            speedups(speeduppath)[1])
    # Create the main matrix. It will have number of activy cols
    # and number of probes rows. 
    lawsonCS = np.zeros((len(magU), len(activity) + 2))
    # Initialise Weibull matrix
    pWeibull = np.zeros((len(magU), len(p)))

    for activityIndex, thisActivity in enumerate(activity): 
        # Get the threshold velocities and frequencies
        uThresh, frequency = criteria(thisActivity, col)
        # A high amplification factor means the threshold wind
        # speed must be lower at that probe, so modify uthresh
        # to acctount for amplification factor
        uThreshMod = uThresh / (magU + 0.00000001)
        pWeibull = np.dot(np.exp(-1 * (uThreshMod / c) ** k), p)
        # fill in separate cols for safety and comfort
        if "safety" in thisActivity:
            lawsonCS[:, 1] =  np.maximum(lawsonCS[:, 1], 
                (pWeibull > frequency) * grade[activityIndex])
            lawsonCS[:, activityIndex + 2] = pWeibull * 8670
        else:
            lawsonCS[:, 0] = np.maximum(lawsonCS[:, 0], 
                (pWeibull > frequency) * grade[activityIndex])
            lawsonCS[:, activityIndex + 2] = pWeibull * 100
    lawsonCSdf = pd.DataFrame(data=lawsonCS, 
                columns=["Comfort", "Safety"] + activity)
    return lawsonCSdf

def plotpolar(datasets, titles, cwd):
    """Creates polar plots of some variable against direction.
    Usually for plotting gem vs mean on the same case, but can
    be called independently to plot any two variables. 

    Parameters
    _____________________________________________________
    datasets: list
        List of datasets to be plotted. Must be a nxd matrix
    where n is no probes and d is 36
    titles: list
        List of titles to match the datasets. len(titles) 
    must be same as len(datasets)
    cwd: string
        Current working directory for filenames

    Returns
    _____________________________________________________
    void: Prints images to file

    """
    print ("Creating polar plots. This may fail if you dont have"
            " an X11 client open...")
    if len(datasets) is not len(titles):
        sys.exit("### ERROR: The number of datasets must equal the"
            " number of titles for polar plots")
    # Create a polar plots directory
    dirname = os.path.join(cwd, "polarPlots")
    if os.path.exists(dirname):
        shutil.rmtree(dirname)
    os.makedirs(dirname)
    # Angle is already 10deg, if this really needs to be changed,
    # can have this as an option. 
    theta = np.arange(0, 360, 10) * np.pi / 180
    theta = np.append(theta, theta[0])
    # Get probe numbers (need this in case probe numbers are not
    # a logical sequence):
    coords = np.genfromtxt(os.path.join(cwd, "coords.csv"), delimiter=",")
    # Create a polar plot for each probe with each of the input args:  
    for probeNo in range(0, len(coords[:,1])-1):
        ax = plt.subplot(111, projection="polar") 
        ax.set_theta_zero_location("N") 
        ax.set_ylim(0, 1.1)
        ax.set_rticks([0.2, 0.4, 0.6, 0.8, 1.0])
        ax.set_rlabel_position(-12.5)
        ax.set_theta_direction(-1)
        ax.grid(True)
        for idx, dataset in enumerate(datasets):
            plotdata = np.append(dataset[probeNo, :], 
                dataset[probeNo, 0])
            ax.plot(theta, plotdata, label=titles[idx])
        ax.legend()
        numberedProbe = coords[probeNo+1, 0]
        ax.set_title("Polar plot for probe "+str(int(numberedProbe)))
        imgname = os.path.join(cwd, "polarPlots/probe_"+
                    str(int(numberedProbe))+".png")
        plt.savefig(imgname, bbox_inches='tight')
        ax.clear()

def plotlawson(lawsonTable, col, season, cwd):
    """Creates lawson plots of the lawsonTable

    Parameters
    _____________________________________________________
    lawsonTable: pd.df
        Dataframe of completed lawson table with coords, 
    probe numbers, gradings.
    col: bool
        Switch to analyse the CoL (True) or LDDC (False)
    season: str
        The seasonal met data to use in the analysis
    cwd: string
        Current working directory for filenames

    Returns
    _____________________________________________________
    void: Prints images to file

    """
    print("Creating Lawson plots for {}".format(season))
    # Create a lawson plots directory
    lawsondir = os.path.join(cwd, "lawsonPlots")
    if not os.path.exists(lawsondir):
        os.makedirs(lawsondir)
    if col:
        colDictComfort = {
            0.0: 'gray', 
            1.5: 'mediumblue', 
            2.5: 'limegreen', 
            3.5: 'gold', 
            4.5: 'firebrick'
        }
        colDictSafety = {
            0.0: 'grey',
            2.5: 'firebrick'
        }
    else:
        colDictComfort = {
            0.0: 'mediumblue', 
            1.5: 'skyblue', 
            2.5: 'limegreen', 
            3.5: 'gold', 
            4.5: 'firebrick'
        }
        colDictSafety = {
            0.0: 'grey',
            1.5: 'gold',
            2.5: 'firebrick',
            3.5: 'black'
        }

    # read the image config file to get scale properties:
    with open("img.yml", "r") as cfgfile:
        cfg = yaml.load(cfgfile)["imgcfg"]
    widthpix = cfg["widthpix"]
    heightpix = cfg["heightpix"]
    resolution = cfg["resolution"] / 25.4 # convert to pix/mm
    border = cfg["border"]
    scale = cfg["scale"]
    xoffset = cfg["xoffset"]
    yoffset = cfg["yoffset"]
    # figure out left point and extents:
    width = (widthpix / resolution * scale) - (2 * border)
    height = (heightpix / resolution * scale) - (2 * border)
    extents = [-width / 2.0, width / 2.0, -height / 2.0, height / 2.0]
    #load in image and plot results on it for each unique z height
    img = plt.imread('background.png')
    for level in lawsonTable.z.unique():
        thisLawsonTable = lawsonTable.loc[lawsonTable['z'] == level]
        xpts = thisLawsonTable['x'] - xoffset
        ypts = thisLawsonTable['y'] - yoffset
        comfortColours = thisLawsonTable['Comfort'].replace(colDictComfort)
        safetyColours = thisLawsonTable['Safety'].replace(colDictSafety)
        plt.imshow(img, extent=extents)
        fs = 60 / resolution #dot/font size is proportional to dpi
        plt.scatter(x=xpts, y=ypts, s=fs, c=comfortColours.values)
        plt.axis('off')
        # Add probe numbers:
        for i, point in thisLawsonTable.iterrows():
            plt.text(xpts[i], ypts[i], str(int(point['probe'])), 
                fontsize=fs)
        # plt.title('Comfort for '+season+' at z ='+str(level))
        plt.savefig(os.path.join(lawsondir, 
            'comfort_'+season+'_z='+str(int(level))+'.png'), 
            dpi=(resolution * 25.4), bbox_inches='tight')
        plt.close()
        if season == 'allyear': 
            plt.imshow(img, extent=extents)
            plt.scatter(x=xpts, y=ypts, s=fs, c=safetyColours.values)
            plt.axis('off')
            for i, point in thisLawsonTable.iterrows():
                plt.text(xpts[i], ypts[i], str(int(point['probe'])), 
                    fontsize=fs)
            plt.savefig(os.path.join(lawsondir,
                'safety_'+'_z='+str(int(level))+'.png'), 
                dpi=(resolution * 25.4), bbox_inches='tight')
            plt.close()

def main():
    parser = argparse.ArgumentParser(description=__doc__, 
                        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-p", "--pplots", action="store_true", dest="pplots", 
                        default=False, help="Switch to print polar plots for "
                        "gem, mean for each direction.")
    parser.add_argument("-c", "--col", action="store_true", dest="col", 
                        default=False, help="Switch to choose to use the CoL "
                        "version of the Lawson criteria.")
    parser.add_argument("-l", "--lplots", action="store_true", dest="lplots", 
                        default=False, help="Switch to choose to enable the "
                        "printing of Lawson plots.")
    args = parser.parse_args()
    # Create some initial useful variables:
    cwd = os.getcwd() 
    casename = os.path.relpath(".", "..")
    user = os.environ.get("USER")

    coords = pd.read_csv(os.path.join(cwd, "coords.csv"))
    metList, metDir = metdata(cwd)
    for season in metList:
        lawsonTable = combinedata(season, metDir, args.col, cwd)
        seasonTable = pd.concat([coords, lawsonTable], axis=1)
        seasonTable.to_csv(os.path.join(cwd, season+"_comfortSafety.csv"), 
            index=False)
        if args.lplots:
            plotlawson(seasonTable, args.col, season, cwd)
    if args.pplots:
        plotpolar([speedups("./speedups")[0], speedups("./speedups")[1]], 
            ['mean', 'gem'], cwd)


if __name__ == "__main__":
    main()
